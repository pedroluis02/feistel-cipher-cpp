//============================================================================
// Name        : Feistel Cipher
// Author      : Pedro Luis
//============================================================================

#include <iostream>
#include <cstring>
#include <cmath>
#include <cstdlib>
using namespace std;

struct bloque_cadena {
	int numero;
	string bloque;
};

const int INICIO_A = 32;
const int FINAL_A  = 126;
const int LONGITUD = (FINAL_A - INICIO_A) + 1;
int ALFABETO_I[LONGITUD];

void iniciar_vector_alfabeto() {
	int k = 0;
	for(int i=INICIO_A; i<=FINAL_A; i++) {
		ALFABETO_I[k] = i;
		k++;
	}
}

int posicion_caracter_alfabeto(char caracter) {
	int ascii = (int)caracter;
    return ((ascii / INICIO_A) - 1) * INICIO_A +
    	   (ascii % INICIO_A);
}

char sustitucion_caracter(char caracter, int valor) {
	int posicion = posicion_caracter_alfabeto(caracter),
	    nueva_posicion = (posicion + valor) % LONGITUD;
	return (char)ALFABETO_I[nueva_posicion];
}

void permutacion_caracteres(string & cadena, int posicion_a, int posicion_b) {
	char aux = cadena[posicion_a];
	cadena[posicion_a] = cadena[posicion_b];
	cadena[posicion_b] = aux;
}

void sustituciones_bloque_caracteres(string & cadena, int valor) {
	int i = 0,
		len = cadena.size();
	while(i < len) {
		cadena[i] = sustitucion_caracter(cadena[i], valor);
		i++;
	}
}

void permutaciones_bloque_caracteres(string & cadena) {
	int len = round((double)cadena.length() / (double)2 ),
	    k = 0,
	    i = 0,
	    j = cadena.length() - 1;
	while(k < len ) {
		permutacion_caracteres(cadena, i, j);
		i++;
		j--;
		k++;
	}
}

int numero_bloques(string cadena, int t_bloque) {
	int i = 0,
		bloque_inicio = 0,
		bloque_final  = 0,
		len = cadena.length() - 1;
	while(bloque_final != len) {
		bloque_inicio = i * t_bloque;
		if((bloque_inicio + (t_bloque - 1)) >= len) {
			bloque_final  = len;
		} else {
			bloque_final  = bloque_inicio + (t_bloque - 1);
		}
		i++;
	}
	return i;
}

struct bloque_cadena  * crear_bloques(string cadena, int n_bloques, int t_bloque, int op) {
	int i = 0,
	    bloque_inicio = 0;
	struct bloque_cadena * bloques = new struct bloque_cadena[n_bloques];
	while(i < n_bloques) {
		if(n_bloques > 1 && op == 1 && i == (n_bloques - 2) && i >= 2) {

			bloques[i].numero = i;
			bloques[i + 1].numero = i + 1;

			bloque_inicio = i * t_bloque;
			int aux = (i + 1) * t_bloque,
			lon = cadena.substr(aux, t_bloque).length();

			aux = bloque_inicio + lon;
			bloques[i].bloque = cadena.substr(bloque_inicio, lon);
			bloques[i + 1].bloque = cadena.substr(aux, t_bloque);

			cout << bloques[i].bloque << " " << bloques[i + 1].bloque << " ";
			break;
		}

		bloque_inicio = i * t_bloque;
		bloques[i].numero = i;

		bloques[i].bloque = cadena.substr(bloque_inicio, t_bloque);

		cout << bloques[i].bloque << " ";
		i++;
	}
	cout << endl;

	return bloques;
}

string sustituciones(struct bloque_cadena * bloques, int n_bloques, int i_bloque, int valor) {
	int i = 0;
	string cadena_sus = "";
	while(i < n_bloques) {
		if(i % 2 == i_bloque) {
			sustituciones_bloque_caracteres(bloques[i].bloque, valor);
		}
		cout << bloques[i].bloque << " ";
		cadena_sus += bloques[i].bloque;
		i++;
	}
	cout << endl;
	return cadena_sus;
}

string permutaciones(struct bloque_cadena * bloques, int n_bloques, int i_bloque) {
	int i = 0;
	string cadena_per = "";
	while(i < n_bloques) {
		if(i % 2 == i_bloque) {
			permutaciones_bloque_caracteres(bloques[i].bloque);
		}
		cout << bloques[i].bloque << " ";
		cadena_per += bloques[i].bloque;
		i++;
	}
	cout << endl;
	return cadena_per;
}

string intercambio_bloques(struct bloque_cadena * bloques, int n_bloques) {
	int k = 0,
	    i = 0,
		j = 1,
	    len = ((double)n_bloques/(double)2);
	string cadena_inter = "";
	while(k < len) {
		swap(bloques[i].numero, bloques[j].numero);
		swap(bloques[i].bloque, bloques[j].bloque);
		i += 2;
		j += 2;
		k++;
	}
	i = 0;
	while(i < n_bloques) {
		cout << bloques[i].bloque << " ";
		cadena_inter += bloques[i].bloque;
		i++;
	}
	cout << endl;
	return cadena_inter;
}

int main(int argc, char ** argv) {
	iniciar_vector_alfabeto();
	string cadena = "Este es el algoritmo de FEISTEL";
	int t_bloque = 8;

	cout << "----------- ALGORITMO DE FEISTEl ----------\n\n";
	cout << "Ingrese cadena: "; getline(cin, cadena);

	if(cadena.length() <= 1) {
		cerr << "Longitud cadena > 1";
		return 0;
	}

	cout << "Ingrese longitud de bloque(> 0): "; cin >> t_bloque;

	if(t_bloque == 0 || t_bloque > (int)cadena.length()) {
		cerr << "\n Longitud bloque debe ser <= longitud cadena y != 0";
		return 0;
	}

	cout << "Original: "<< cadena << "\n\n";

	int n_bloques = numero_bloques(cadena, t_bloque);
	struct bloque_cadena *bloques = crear_bloques(cadena, n_bloques, t_bloque, 0);

	// Encriptacion
	int valor = 1;

	sustituciones(bloques, n_bloques, 0, valor);
	permutaciones(bloques, n_bloques, 0);

	intercambio_bloques(bloques, n_bloques);

	sustituciones(bloques, n_bloques, 0, valor);
    string cadena_encriptada = permutaciones(bloques, n_bloques, 0);

    cout << endl << "Encriptado: "<< cadena_encriptada << "\n\n";
    delete []bloques;

    // Desencriptacion
    valor = -1;
    n_bloques = numero_bloques(cadena_encriptada, t_bloque);
    struct bloque_cadena * bloques_encriptado = crear_bloques(cadena_encriptada, n_bloques, t_bloque, 1);

    permutaciones(bloques_encriptado, n_bloques, 0);
    sustituciones(bloques_encriptado, n_bloques, 0, valor);

    intercambio_bloques(bloques_encriptado, n_bloques);

    permutaciones(bloques_encriptado, n_bloques, 0);
    string cadena_desencriptada = sustituciones(bloques_encriptado, n_bloques, 0, valor);

    cout << endl << "Desencriptado: "<< cadena_desencriptada << "\n\n";

	delete []bloques_encriptado;
	return 0;
}
